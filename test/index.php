<?php
use TeamSportia\WP_Proxy;
use TeamSportia\Settings;

require( dirname( __FILE__ ) . '/../wp-blog-header.php' );
require( dirname( __FILE__ ) . '/../service/team-sportia/src/Settings.php' );
require( dirname( __FILE__ ) . '/../service/team-sportia/src/WP_Proxy.php' );
require( dirname( __FILE__ ) . '/../wp-blog-header.php' );
call_user_func(function() {
	// Om du vill använda WordPress db-interface
	/* @var WPDB $wpdb */
	global $wpdb;

	status_header(200);

	// Om du vill använda PDO
	$pdo = (new WP_Proxy())->getPdo();
	//$results = $pdo->query("SELECT * FROM ts_site_collection")->fetchAll(PDO::FETCH_ASSOC);

	// Om du vill använda traditionell MySQLi
	$mysqli = mysqli_connect(Settings::DbHost, Settings::DbUser, Settings::DbPass, Settings::DbName);

	// Om du vill använda objektorienterad MySQLi
	$mysqliObj = new mysqli(Settings::DbHost, Settings::DbUser, Settings::DbPass, Settings::DbName);

	$results = $wpdb->get_results("SELECT * FROM ts_site_collection");

	// ts_site_collection stämmer inte. Töm den tabellen
	// Hämta ut rätt information från bloggarna och sätt in den informationen i ts_site_collection igen
	// Information om bloggarna finns i ts_blogs
	// ts_x_options - där finns store_id som måste hämtas

	header('Content-Type: application/json');
	echo json_encode($results);



});