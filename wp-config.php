<?php
/**
 * Baskonfiguration för WordPress.
 *
 * Denna fil innehåller följande konfigurationer: Inställningar för MySQL,
 * Tabellprefix, Säkerhetsnycklar, WordPress-språk, och ABSPATH.
 * Mer information på {@link http://codex.wordpress.org/Editing_wp-config.php 
 * Editing wp-config.php}. MySQL-uppgifter får du från ditt webbhotell.
 *
 * Denna fil används av wp-config.php-genereringsskript under installationen.
 * Du behöver inte använda webbplatsen, du kan kopiera denna fil direkt till
 * "wp-config.php" och fylla i värdena.
 *
 * @package WordPress
 */

// ** MySQL-inställningar - MySQL-uppgifter får du från ditt webbhotell ** //
/** Namnet på databasen du vill använda för WordPress */
define('DB_NAME', 'raketnu_ts_butiker');

/** MySQL-databasens användarnamn */
define('DB_USER', 'raketnu_ts_butik');

/** MySQL-databasens lösenord */
define('DB_PASSWORD', 'h7&pm8D20~8[');

/** MySQL-server */
define('DB_HOST', 'raket.nu');

/** Teckenkodning för tabellerna i databasen. */
define('DB_CHARSET', 'utf8');

/** Kollationeringstyp för databasen. Ändra inte om du är osäker. */
define('DB_COLLATE', '');

/**#@+
 * Unika autentiseringsnycklar och salter.
 *
 * Ändra dessa till unika fraser!
 * Du kan generera nycklar med {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Du kan när som helst ändra dessa nycklar för att göra aktiva cookies obrukbara, vilket tvingar alla användare att logga in på nytt.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~<8.>BiY7y`k77B?YB[=aeVX4?-N*A)|Jsc 8q:au8o@@>^7jl-sWsB0Pwh<Ek:3');
define('SECURE_AUTH_KEY',  '|6 1|+i11M!{=z3(H[M;Z&ohZT^We]) {_4y<V;d1|T|J^J+<.DI;dC+ a?zXqAX');
define('LOGGED_IN_KEY',    '&4d;Na?Vz[N2uR2yq2sQ5h/k(<X{*,5U>|j[-6HDn,5c_X vV1)UyhRyh+h|O2BI');
define('NONCE_KEY',        'UR|wQWEr.=`D|!#+L 9v!;+]tMnV xcloW:IGeG~&MD4%:~otGD/AMA=RO<&cqcr');
define('AUTH_SALT',        'QM|n-+;QQ/aq)8aQY^F4 `sBGt-CA+~Whiii9A+vl[uqGgoy8j;cDM@hneFMh-v4');
define('SECURE_AUTH_SALT', 'gwHO|<N<|l;IUPX1)Q)3 yf$!cn.3loHuZ^P)9tZ:l&dl.(jnqlT|i;$jZrMa3j3');
define('LOGGED_IN_SALT',   '2;{:(5;tj48zj}++<KqSE}{UXI[L-CE,e]9;W{x! pK8LE,;9e-73f`cx?pgUoz_');
define('NONCE_SALT',       'K;_$f}8>s5fHU-RbV@k,s-p$k(0_I0:Ww+,%OZk5yB.*]IMy_k6j6 _)?X-EnV`^');

/**#@-*/

/**
 * Tabellprefix för WordPress Databasen.
 *
 * Du kan ha flera installationer i samma databas om du ger varje installation ett unikt
 * prefix. Endast siffror, bokstäver och understreck!
 */
$table_prefix  = 'ts_';

/** 
 * För utvecklare: WordPress felsökningsläge. 
 * 
 * Ändra detta till true för att aktivera meddelanden under utveckling. 
 * Det är rekommderat att man som tilläggsskapare och temaskapare använder WP_DEBUG 
 * i sin utvecklingsmiljö. 
 */ 
define('WP_DEBUG', false);

/* Det var allt, sluta redigera här! Blogga på. */

/** Absoluta sökväg till WordPress-katalogen. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Anger WordPress-värden och inkluderade filer. */
require_once(ABSPATH . 'wp-settings.php');