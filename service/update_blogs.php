<?php
error_reporting(E_ALL);
ini_set('display_errors', 2);
ini_set('memory_limit', '512M');
date_default_timezone_set("UTC");
require "../vendor/autoload.php";

if (isset($_GET["token"]) && $_GET["token"] == base64_encode(\TeamSportia\Settings::Token)) {
	$pdo = (new \TeamSportia\WP_Proxy())->getPdo();
	$pdo->exec("TRUNCATE TABLE ts_site_collection");
	$blogs = $pdo->query("SELECT * FROM ts_blogs")->fetchAll(PDO::FETCH_ASSOC);
	foreach ($blogs as $blog) {
		$blogId = $blog["blog_id"];
		$storeData = $pdo->query("SELECT * FROM ts_{$blogId}_options WHERE option_name = 'store_id'");
		if (is_object($storeData)) {
			$storeData = $storeData->fetch(PDO::FETCH_ASSOC);
			$storeId = $storeData["option_value"];
			$pdo->exec("INSERT INTO ts_site_collection (site_id, store_id) VALUES ($blogId, $storeId)");
		}
	}
	echo "Stores updated";
}
else {
	echo "This is not the service you're looking for<br/>";
	//echo base64_encode(\TeamSportia\Settings::Token);
}