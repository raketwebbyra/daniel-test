<?php

/**
 * Store Service
 *
 * To create a Ping Request (store list);
 * /service/?type=PingRequest
 *
 * To create a Ping Request (single store, with ID);
 * /service/?type=PingRequest&storeId=<ID>
 *
 * When fetching from service (store list);
 * /service/?type=ExportService
 *
 * When fetching from service (single store, with ID);
 * /service/?type=ExportService&storeId=<ID>
 *
 */

use TeamSportia\Delete_Request;
use TeamSportia\Export_Response_Type;
use TeamSportia\Ping_Request;
use TeamSportia\Ping_Request_Type;
use TeamSportia\Service_Query_Parser;
use TeamSportia\Settings;
use TeamSportia\Store_Export;

error_reporting(E_ALL);
ini_set('display_errors', 2);
ini_set('memory_limit', '512M');
date_default_timezone_set("UTC");
require "../vendor/autoload.php";

try {
	$query = new Service_Query_Parser($_GET);
	if ($query->isPingService()) {
		$type = $query->isSingleStore() ? Ping_Request_Type::SingleStorePing() : Ping_Request_Type::StoreListPing();
		$request = new Ping_Request($type);
		if ($query->isSingleStore() && !$query->isValidSingleStoreRequest()) {
			throw new Exception("No Store Id Provided");
		}
		else {
			if ($query->isSingleStore()) {
				$request->setStoreId($query->getStoreId());
				$request->setBlogId($query->getBlogId());
			}
		}
		$request->send();
	}

	if ($query->isExportService()) {
		$type = $query->isSingleStore() ? Export_Response_Type::SingleStoreExport() : Export_Response_Type::StoreListExport();
		$export = new Store_Export($type);
		if ($query->isSingleStore() && !$query->isValidSingleStoreRequest()) {
			throw new Exception("No Store Id Provided");
		}
		else {
			if ($query->isSingleStore()) {
				$export->setStoreId($query->getStoreId());
				$export->setBlogId($query->getBlogId());
			}
		}
		header("Content-Type: application/json; charset=UTF-8");
		$export->export();
	}

	if ($query->isDeleteService() && isset($_GET["token"]) && $_GET["token"] == base64_encode( Settings::Token)) {
		$request = new Delete_Request(Ping_Request_Type::SingleStorePing());
		$request->setStoreId($query->getStoreId());
		$request->setBlogId($query->getBlogId());
		$request->send();
	}
} catch (Exception $e) {
	header("HTTP/1.1 400 Bad Request");
	header("Content-Type: text/html; charset=UTF-8");
	echo $e->getMessage();
	die();
}