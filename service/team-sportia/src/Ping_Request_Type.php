<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-12-17
 * Time: 20:56
 */

namespace TeamSportia;


class Ping_Request_Type {
	private $_type = 0;

	private function __construct($type) {
		$this->_type = $type;
	}

	public function getType() {
		return $this->_type;
	}

	public static function SingleStorePing() {
		return new Ping_Request_Type(1);
	}

	public static function StoreListPing() {
		return new Ping_Request_Type(2);
	}
}