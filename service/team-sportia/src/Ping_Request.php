<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-12-17
 * Time: 20:54
 */

namespace TeamSportia;

use Exception;
use GuzzleHttp\Client;

class Ping_Request {

	protected $_requestType = 0;
	protected $_storeId = 0;
	protected $_blogId = 0;

	public function __construct(Ping_Request_Type $type) {
		$this->_requestType = $type;
	}

	public function setStoreId($storeId) {
		$this->_storeId = $storeId;
	}

	/**
	 * @param int $blogId
	 */
	public function setBlogId($blogId) {
		$this->_blogId = $blogId;
	}

	public function send() {

		$token = $this->_generateToken();
		$data = ["Token" => $token];

		if (Ping_Request_Type::SingleStorePing()
				->getType() === $this->_requestType->getType()
		) {
			if ($this->_storeId === 0 && $this->_blogId === 0) {
				throw new Exception('No Store Id Specified');
			}
			$data = array_merge($data, [
				"IsSingleStore" => TRUE,
				"SingleStoreId" => $this->_storeId,
				"BlogId" => $this->_blogId
			]);
		}
		else {
			$data = array_merge($data, [
				"IsSingleStore" => FALSE,
				"IsStoreList" => TRUE
			]);
		}

		header("Content-Type: application/json; charset=UTF-8");
		//$client = new Client();
		//$response = $client->post(Settings::PingRequestUrl, ["json" => ["Request" => $data]]);
		//echo $response->getBody();
		$response = $this->sendRequest(Settings::PingRequestUrl, $data);
		$responseData = (array)json_decode($response);
		//$responseData["localToken"] = Settings::Token;
		echo json_encode($responseData);
	}

	protected function sendRequest($url, $data) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL,            $url);
		curl_setopt($curl, CURLOPT_POST,           1);
		curl_setopt($curl, CURLOPT_POSTFIELDS,     json_encode(["Request" => $data]));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER,     array('Content-Type: application/json; charset=UTF-8'));

		$response = curl_exec($curl);
		if ($response === false) {
			var_dump($response, json_encode($data),curl_error($curl));
			die();
		}
		curl_close($curl);
		return $response;
	}

	protected function _generateToken() {
		$now = time();
		$token = Settings::Token . ":";
		$token .= mktime(date('H', $now), 0, 0, date('m', $now), date('d', $now), date('Y', $now));
		return base64_encode(Settings::Token);
	}
}