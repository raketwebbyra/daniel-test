<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-12-17
 * Time: 20:55
 */

namespace TeamSportia;

class Export_Response_Type {
	private $_type = 0;

	private function __construct($type) {
		$this->_type = $type;
	}

	public function getType() {
		return $this->_type;
	}

	public static function SingleStoreExport() {
		return new Export_Response_Type(1);
	}

	public static function StoreListExport() {
		return new Export_Response_Type(2);
	}
}