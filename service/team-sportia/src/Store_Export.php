<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-12-17
 * Time: 20:54
 */

namespace TeamSportia;

class Store_Export {

	private $_exportType = 0;
	private $_storeId = 0;
	private $_blogId = 0;

	public function __construct(Export_Response_Type $type) {
		$this->_exportType = $type;
	}

	public function setStoreId($storeId) {
		$this->_storeId = $storeId;
	}

	/**
	 * @param int $blogId
	 */
	public function setBlogId($blogId) {
		$this->_blogId = $blogId;
	}

	private $optionFields = [
		'siteurl' => 'URL',
		'blogname' => 'Name',
		'store_lat' => 'Latitude',
		'store_lon' => 'Longitude',
		'store_street_address' => 'Address',
		'store_city' => 'City',
		'store_post_code' => 'Zip',
		'store_search_words' => 'QueryableNames',
	];

	public function export() {
		$proxy = new WP_Proxy();
		if (Export_Response_Type::SingleStoreExport()
				->getType() === $this->_exportType->getType()
		) {
			$proxy->setStoreId($this->_storeId);
			$proxy->setBlogId($this->_blogId);
		}

		$data = $proxy->get();
		$exported = array();

		foreach ($data as $storeData) {

			$storeExport = array();
			$storeExport['Id'] = $storeData['storeId'];
			$storeExport['BlogId'] = $storeData['referenceId'];

			foreach ($storeData['data'] as $row) {
				if ($row['option_name'] == "store_lat" || $row['option_name'] == "store_lon") {
					$storeExport[$this->optionFields[$row['option_name']]] = null;
					if (trim($row['option_value']))
						$storeExport[$this->optionFields[$row['option_name']]] = (float)trim($row['option_value']);
					continue;
				}
				if (isset($this->optionFields[$row['option_name']])) {
					$storeExport[$this->optionFields[$row['option_name']]] = trim($row['option_value']);
					continue;
				}

				if ($row['option_name'] == 'store_open_list') {
					$values = unserialize($row['option_value']);
					$storeExport['OpenHours'] = array();
					for ($i = 0; $i < count($values); $i++) {
						$storeInfo = [
							'SortOrder' => ($i + 1),
							'Name' => trim($values[$i]['open-day']),
							'Value' => trim($values[$i]['open-hours']),
						];
						$storeInfo['VisibleFrom'] =  isset($values[$i]['from']) ? trim($values[$i]['from']) : null;
						$storeInfo['VisibleTo'] =  isset($values[$i]['to']) ? trim($values[$i]['to']) : null;
						$storeExport['OpenHours'][] = $storeInfo;
					}
				}
			}

			$events = $proxy->getEvents($storeData['referenceId']);
			$storeExport['Events'] = array();
			for ($i = 0; $i < count($events); $i++) {
				$storeExport['Events'][] = array(
					'Title' => trim($events[$i]['post_title']),
					'Message' => trim($events[$i]['post_content']),
					'Start' => $events[$i]['dt_converted_start'],
					'End' => $events[$i]['dt_converted_end'],
					'EventHiddenOn' => (bool)$events[$i]['event_hidden_on'],
				);
			}
			$exported[] = $storeExport;
		}

		if (Export_Response_Type::SingleStoreExport()
				->getType() === $this->_exportType->getType()
		) {
			$exported = $exported[0];
		}

		echo json_encode($exported);
		die();
	}
}