<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-12-17
 * Time: 20:55
 */

namespace TeamSportia;

use Exception;
use PDO;

class WP_Proxy {

	private $_pdh;
	private $_storeId = FALSE;
	private $_blogId = FALSE;

	public function __construct() {

		$this->_pdh = new PDO("mysql:host=" . Settings::DbHost . ";dbname=" . Settings::DbName, Settings::DbUser, Settings::DbPass);
		$this->_pdh->query("set names 'utf8'");
		$this->_pdh->query("set character set 'utf8' collate 'utf8_swedish_ci'");

	}

	public function setStoreId($storeId) {
		$this->_storeId = $storeId;
	}

	/**
	 * @param boolean $blogId
	 */
	public function setBlogId($blogId) {
		$this->_blogId = $blogId;
	}

	private function isSingleStore() {
		return ($this->_storeId || $this->_blogId);
	}

	private function getSingleStoreSql() {
		$sql = ($this->_blogId) ? "`site_id`" : "`store_id`";
		return [
			"sql" => " WHERE ".$sql."=:id",
			"data" => [':id' => $this->_blogId ?: $this->_storeId]
		];
	}

	public function get() {
		$stores = array();
		$sql = "SELECT * FROM `ts_site_collection`";
		$sqlData = [];
		$data = [];
		if ($this->isSingleStore()) {
			$ssSql = $this->getSingleStoreSql();
			$sql .= $ssSql["sql"];
			$sqlData = $ssSql["data"];
		}
		$sql .= " GROUP BY store_id, site_id ORDER BY site_id";
		$statement = $this->_pdh->prepare($sql);
		$statement->execute($sqlData);
		$list = $statement->fetchAll(PDO::FETCH_ASSOC);
		if ($this->_storeId && count($list) == 0) {
			throw new Exception("No Such Store");
		}

		foreach ($list as $item) {
			$stores[] = array(
				'siteId' => $item['site_id'],
				'storeId' => $item['store_id']
			);
		}
		unset($list);
		foreach ($stores as $reference) {
			$data[] = array(
				'storeId' => $reference['storeId'],
				'referenceId' => $reference['siteId'],
				'data' => $this->_pdh->query("
                	SELECT *
                	FROM `ts_" . $reference['siteId'] . "_options`
                ")->fetchAll(PDO::FETCH_ASSOC)
			);
		}

		return $data;
	}

	public function getEvents($referenceId) {
		return $this->_pdh->query("
        	SELECT
        		p.`post_title` AS post_title,
        		p.`post_content` AS post_content,
        		from_unixtime(convert(pm1.`meta_value`, UNSIGNED INTEGER)) AS dt_converted_start,
        		from_unixtime(convert(pm2.`meta_value`, UNSIGNED INTEGER)) AS dt_converted_end,
        		pm3.meta_value AS event_hidden_on
        	FROM
        		`ts_" . $referenceId . "_postmeta` AS pm1
        		INNER JOIN
        			`ts_" . $referenceId . "_postmeta` AS pm2
        		ON
        			pm1.`post_id`=pm2.`post_id`
        			AND
        			pm2.`meta_key`='ts_event_ends'
        		INNER JOIN
        			`ts_" . $referenceId . "_postmeta` AS pm3
        		ON
        			pm1.`post_id`=pm3.`post_id`
        			AND
        			pm3.`meta_key`='ts_event_hidden_on'
        		INNER JOIN
        			`ts_" . $referenceId . "_posts` AS p
        		ON
        			p.`id`=pm1.`post_id`
        	WHERE
        		pm1.`meta_key`='ts_event_starts'
        		AND
        		convert(pm1.`meta_value`, UNSIGNED INTEGER) >= unix_timestamp()
        	ORDER BY
        		convert(pm1.`meta_value`, UNSIGNED INTEGER) ASC
        ")->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getPdo() {
		return $this->_pdh;
	}
}