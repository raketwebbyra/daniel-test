<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2014-12-17
 * Time: 20:51
 */

namespace TeamSportia;
use Exception;

class Service_Query_Parser {

	private $_services = array('PingRequest' => 1, 'ExportService' => 2, 'DeleteService' => 3);
	private $_type = 0;
	private $_isSingleStore = FALSE;
	private $_storeId = "";
	private $_blogId = "";

	public function __construct(array $params) {
		if (isset($params['type']) && array_key_exists($params['type'], $this->_services)) {
			$this->_type = $this->_services[$params['type']];
		}
		else {
			throw new Exception("No Such Service");
		}
		$this->_isSingleStore = isset($params['storeId']) || isset($params["blogId"]);
		if (isset($params['storeId']) && intval($params['storeId']) > 0) {
			$this->_storeId = intval($params['storeId']);
		}

		if (isset($params['blogId']) && intval($params['blogId']) > 0) {
			$this->_blogId = intval($params['blogId']);
		}
	}

	public function isPingService() {
		return $this->_type === $this->_services['PingRequest'];
	}

	public function isExportService() {
		return $this->_type === $this->_services['ExportService'];
	}

	public function isSingleStore() {
		return $this->_isSingleStore;
	}

	public function getStoreId() {
		return $this->_storeId;
	}

	public function getBlogId() {
		return $this->_blogId;
	}

	public function isValidSingleStoreRequest() {
		return ($this->getStoreId() != "" || $this->getBlogId() != "");
	}

	public function isDeleteService() {
		return $this->_type === $this->_services['DeleteService'];
	}
}