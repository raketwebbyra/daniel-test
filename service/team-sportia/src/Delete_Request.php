<?php
/**
 * Created by PhpStorm.
 * User: Marcus Dalgren
 * Date: 2015-02-02
 * Time: 00:59
 */

namespace TeamSportia;


class Delete_Request extends Ping_Request {
	public function send() {
		$token = $this->_generateToken();
		$data = [
			"Token" => $token,
			"IsSingleStore" => TRUE,
			"SingleStoreId" => 0,
			"BlogId" => $this->_blogId,
			"IsStoreList" => FALSE
		];

		header("Content-Type: application/json; charset=UTF-8");
		$response = $this->sendRequest(Settings::DeleteRequestUrl, $data);
		$responseData = (array)json_decode($response);
		echo json_encode($responseData);
	}
}