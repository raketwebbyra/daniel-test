<?php

require_once "settings.php";


class Service_Query_Parser {

    private $_services = array('PingRequest' => 1, 'ExportService' => 2);
    private $_type = 0;
    private $_isSingleStore = false;
    
    private $_storeId = "";
    
    public function __construct(array $params) {
        
        if (isset($params['type']) && array_key_exists($params['type'], $this->_services)) {
            
            $this->_type = $this->_services[$params['type']];
            
        } else {
            
            throw new Exception("No Such Service");
            
        }
        
        if (isset($params['storeId'])) {
            
            $this->_isSingleStore = true;
            
        }
        
        if (isset($params['storeId']) && intval($params['storeId']) > 0) {
            
            $this->_storeId = intval($params['storeId']);
            
        }
        
    }
    
    public function isPingService() {
        return $this->_type === $this->_services['PingRequest'];
    }
    
    public function isExportService() {
        return $this->_type === $this->_services['ExportService'];
    }
    
    public function isSingleStore() {
        return $this->_isSingleStore;
    }
    
    public function getStoreId() {
        return $this->_storeId;
    }
    
}

class Store_Export {
    
    private $_exportType = 0;
    private $_storeId = 0;
    
    public function __construct(Export_Response_Type $type) {
        
        $this->_exportType = $type;
        
    }
    
    public function setStoreId($storeId) {
        
        $this->_storeId = $storeId;
        
    }
    
    public function export() {
        
        $proxy = new WP_Proxy();
        
        if (Export_Response_Type::SingleStoreExport()->getType() === $this->_exportType->getType()) {
            
            $proxy->setStoreId($this->_storeId);
            
        }
        
        $data = $proxy->get();
        $exported = array();
        
        foreach ($data as $storeData) {
            
            $storeExport = array();
            $storeExport['Id'] = $storeData['storeId'];
            
            foreach ($storeData['data'] as $row) {
                
                if ($row['option_name'] == 'siteurl') {              $storeExport['URL'] = trim($row['option_value']); }
                if ($row['option_name'] == 'blogname') {             $storeExport['Name'] = trim($row['option_value']); }
                if ($row['option_name'] == 'store_lat') {            $storeExport['Latitude'] = $row['option_value']; }
                if ($row['option_name'] == 'store_lon') {            $storeExport['Longitude'] = $row['option_value']; }
                if ($row['option_name'] == 'store_street_address') { $storeExport['Address'] = trim($row['option_value']); }
                if ($row['option_name'] == 'store_city') {           $storeExport['City'] = trim($row['option_value']); }
                if ($row['option_name'] == 'store_post_code') {      $storeExport['Zip'] = trim($row['option_value']); }
                if ($row['option_name'] == 'store_search_words') {   $storeExport['QueryableNames'] = trim($row['option_value']); }
                
                if ($row['option_name'] == 'store_open_list') {
    	            
    	            $values = unserialize($row['option_value']);
    	            $storeExport['OpenHours'] = array();
    	            
    	            for ($i = 0; $i < sizeof($values); $i++) {
    		            
    		            array_push($storeExport['OpenHours'], array(
    		            	'SortOrder' => ($i + 1),
    		            	'Name'      => trim($values[$i]['open-day']),
    		            	'Value'     => trim($values[$i]['open-hours'])
    		            ));
    		            
    	            }
    	            
                }
            
            }
                    
            $events = $proxy->getEvents($storeData['referenceId']);
            $storeExport['Events'] = array();
                
            for ($i = 0; $i < sizeof($events); $i++) {
                
                array_push($storeExport['Events'], array(
                    'Title'   => trim($events[$i]['post_title']),
                    'Message' => trim($events[$i]['post_content']),
                    'Start'   => $events[$i]['dt_converted_start'],
                    'End'     => $events[$i]['dt_converted_end']
                ));
                
            }
            
            $exported[] = $storeExport;
            
        }
        
        if (Export_Response_Type::SingleStoreExport()->getType() === $this->_exportType->getType()) {
            
            $exported = $exported[0];
            
        }
        
        echo json_encode($exported);
        die();
        
    }
    
}

class Ping_Request {
    
    private $_requestType = 0;
    private $_storeId = 0;
    
    public function __construct(Ping_Request_Type $type) {
        
        $this->_requestType = $type;
        
    }
    
    public function setStoreId($storeId) {
        
        $this->_storeId = $storeId;
        
    }
    
    public function send() {
        
        $token = $this->_generateToken();
        $data = "";
        
        if (Ping_Request_Type::SingleStorePing()->getType() === $this->_requestType->getType()) {
            
            if ($this->_storeId === 0) {
                throw new Exception('No Store Id Specified');
            }
            
            $data = '{"Request": {"Token": "' . $token . '", "IsSingleStore": true, "SingleStoreId": ' . $this->_storeId . '}}';
            
        } else {
            
            $data = '{"Request": {"Token": "' . $token . '", "IsSingleStore": false, "IsStoreList": true}}';
            
        }
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,            Settings::PingRequestUrl);
        curl_setopt($curl, CURLOPT_POST,           1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,     $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER,     array('Content-Type: application/json; charset=UTF-8'));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        
        header("Content-Type: application/json; charset=UTF-8");
        echo $response;
        
    }
    
    private function _generateToken() {
        
        $now    = time();
        
        $token  = Settings::Token . ":";
        $token .= mktime(date('H', $now), 0, 0, date('m', $now), date('d', $now), date('Y', $now));
        
        return base64_encode($token);
        
    }
    
}

class WP_Proxy {
    
    private $_pdh;
    private $_storeId = false;
    
    public function __construct() {
        
        $this->_pdh = new PDO(
                    "mysql:host=" . Settings::DbHost . ";dbname=" . Settings::DbName,
                    Settings::DbUser,
                    Settings::DbPass
            );
        $this->_pdh->query("set names 'utf8'");
        $this->_pdh->query("set character set 'utf8' collate 'utf8_swedish_ci'");
        
    }
    
    public function setStoreId($storeId) {
        $this->_storeId = $storeId;
    }
    
    public function get() {
    
        $stores = array();
    
        if ($this->_storeId) {
        
            $statement = $this->_pdh->prepare("
            	select
            		*
            	from
            		`ts_site_collection`
            	where
            		`store_id`=:storeId
            ");
            $statement->execute(
            	array(
            		':storeId' => $this->_storeId
            	)
            );
            $collected = $statement->fetch(PDO::FETCH_ASSOC);
            
            if (!$collected) {
                
                throw new Exception("No Such Store");
                
            }
            
            $stores[] = array(
                'siteId'  => $collected['site_id'],
                'storeId' => $this->_storeId
            );
            
        } else {
            
            $list = $this->_pdh->query("
                select
                    *
                from
                    `ts_site_collection`
            ")->fetchAll(PDO::FETCH_ASSOC);
            
            foreach ($list as $item) {
                $stores[] = array(
                    'siteId' => $item['site_id'],
                    'storeId' => $item['store_id']
                );
            }
            unset($list);
            
        }
        
        $data = array();
        
        foreach ($stores as $reference) {
        
            $data[] = array(
                'storeId'     => $reference['storeId'],
                'referenceId' => $reference['siteId'],
                'data'        => $this->_pdh->query("
                	select
                		*
                	from
                		`ts_" . $reference['siteId'] . "_options`
                ")->fetchAll(PDO::FETCH_ASSOC)
            );
        
        }
        
        return $data;
            
    }
    
    public function getEvents($referenceId) {
        
        return $this->_pdh->query("
        	select
        		p.`post_title` as post_title,
        		p.`post_content` as post_content,
        		from_unixtime(convert(pm1.`meta_value`, unsigned integer)) as dt_converted_start,
        		from_unixtime(convert(pm2.`meta_value`, unsigned integer)) as dt_converted_end
        	from
        		`ts_" . $referenceId . "_postmeta` as pm1
        		inner join
        			`ts_" . $referenceId . "_postmeta` as pm2
        		on
        			pm1.`post_id`=pm2.`post_id`
        			and
        			pm2.`meta_key`='ts_event_ends'
        		inner join
        			`ts_" . $referenceId . "_posts` as p
        		on
        			p.`id`=pm1.`post_id`
        	where
        		pm1.`meta_key`='ts_event_starts'
        		and
        		convert(pm1.`meta_value`, unsigned integer) >= unix_timestamp()
        	order by
        		convert(pm1.`meta_value`, unsigned integer) asc
        	limit 2
        ")->fetchAll(PDO::FETCH_ASSOC);
        
    }
    
}

class Export_Response_Type {
    
    private $_type = 0;
    
    private function __construct($type) {
        $this->_type = $type;
    }
    
    public function getType() {
        return $this->_type;
    }
    
    public static function SingleStoreExport() {
        return new Export_Response_Type(1);
    }
    
    public static function StoreListExport() {
        return new Export_Response_Type(2);
    }
    
}

class Ping_Request_Type {
    
    private $_type = 0;
    
    private function __construct($type) {
        $this->_type = $type;
    }
    
    public function getType() {
        return $this->_type;
    }
    
    public static function SingleStorePing() {
        return new Ping_Request_Type(1);
    }
    
    public static function StoreListPing() {
        return new Ping_Request_Type(2);
    }
    
}